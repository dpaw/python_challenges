import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delware': 'Dover',
    'Florida': 'Talahassee',
    'Georgia': 'Atlanta',
    }

result = random.choice(list(capitals_dict.items()))

print(result[0])

while True:
    response = input("What is the capital of this state? ").lower()
    if response == "exit":
        print(f"{result[1]}, goodbye")
        break
    elif response == result[1].lower():
        print("Correct")
        break
