def invest(amount, rate, years):
    """Information on the annual rate of return"""
    for n in range(1, years + 1):
        amount = amount * (1 + rate)
        print(f"year {n}: ${amount:.2f}")

amount = float(input("Enter amount: "))
rate = float(input("Enter rate: "))
years = int(input("Enter years: "))

invest(amount, rate, years)
