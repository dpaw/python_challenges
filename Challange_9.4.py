universities = [
    ["California Institute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachusetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500],
]

Total_students = []
Total_tuition = []

def enrollment_stats(list_of_universities):
    for i in list_of_universities:
        Total_students.append(i[1])
        Total_tuition.append(i[2])
    return Total_students, Total_tuition

def mean(data):
    return sum(data) / len(data)

def median(data):
    data.sort()
    if len(data) % 2 == 1:
        median_element = int(len(data) / 2)
        return data[median_element]
    else:
        median_element_left = ((len(list_of_universities)) - 1 ) // 2
        median_element_right = ((len(list_of_universities)) + 1) // 2
        return mean(data[median_element_left], data[median_element_right])

totals = enrollment_stats(universities)

print("*" * 30)
print(f"Total students:  {sum(totals[0]):,}")
print(f"Total tuition: $ {sum(totals[1]):,}")
print(f"\nStudent mean:   {mean(totals[0]):,.2f}")
print(f"Student median: {median(totals[0]):,}")
print(f"\nTuition mean:   $ {mean(totals[1]):,.2f}")
print(f"Tuition median: $ {median(totals[1]):,}")
print("*" * 30)

