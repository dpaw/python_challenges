import random

def flip_of_coin():
    """Heads or tails - randomly return one"""
    if random.randint(0,1) == 0:
        return "Heads"
    else:
        return "Tails"

num_of_trials = 10_000
flips = 0

for trial in range(num_of_trials):
    if flip_of_coin() == "Heads":
        flips = flips + 1
        while flip_of_coin() == "Heads":
            flips = flips + 1
        flips = flips + 1
    else:
        flip_of_coin() == "Tails"
        flips = flips + 1
        while flip_of_coin() == "Tails":
            flips = flips + 1
        flips = flips + 1

print(f"The average number of flips per trial is {flips / num_of_trials}")
