import random

noun = [
    "fossil",
    "horse",
    "aardvark",
    "judge",
    "chef",
    "mango",
    "extrovert",
    "gorilla",]

verb = [
    "kicks",
    "jingles",
    "bounces",
    "slurps",
    "meows",
    "explodes",
    "curdles",
]
adjective = [
    "furry",
    "balding",
    "incredulous",
    "fragrant",
    "exuberant",
    "glistening",
]
preposition = [
    "against",
    "after",
    "into",
    "beneath",
    "upon",
    "for",
    "in",
    "like",
    "over",
    "within",
]
adverb = [
    "curiously",
    "extravagantly",
    "tantalizingly",
    "furiously",
    "sensuously",
]

def poem():
    a1 = random.choice(noun)
    a2 = random.choice(noun)
    a3 = random.choice(noun)
    while a2 == a1:
        a2 = random.choice(noun)
    while a3 == a1 or a3 == a2:
        a3 = random.choice(noun)     
        
    b1 = random.choice(verb)
    b2 = random.choice(verb)
    b3 = random.choice(verb)
    while b2 == b1:
        b2 = random.choice(verb)
    while b3 == b1 or b3 == b2:
        b3 = random.choice(verb)   

    c1 = random.choice(adjective)
    c2 = random.choice(adjective)
    c3 = random.choice(adjective)
    while c2 == c1:
        c2 = random.choice(adjective)
    while c3 == c1 or c3 == c2:
        c3 = random.choice(adjective)

    d1 = random.choice(preposition)
    d2 = random.choice(preposition)
    while d2 == d1:
        d2 = random.choice(preposition)
    
    e1 = random.choice(adverb)

    vowel = "aeiou"

    if vowel.find(c1[0]) != -1:
        article = "An"
    else:
        article = "A"

    print_poem = (
        f"{article} {c1} {a1}\n"
        f"\n"
        f"{article} {c1} {a1} {b1} {d1} the {c2} {a2}\n"
        f"{e1}, the {a1} {b2}\n"
        f"the {a2} {b3} {d2} a {c3} {a3}")
        
    return print_poem
    
print_poem = poem()
print(print_poem)



    
