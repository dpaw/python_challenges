number = int(input("Give me a positive integer: "))
for divider in range(1, number + 1):
    if number % divider == 0:
        print(f"{divider} is a factor of {number}")
