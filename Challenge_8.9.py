from random import random

A_wins = 0
B_wins = 0
num_of_trials = 10_000

for trial in range(num_of_trials):
    A_votes = 0
    B_votes = 0
    if random() < 0.87:
        A_votes = A_votes + 1
    else:
        B_votes = B_votes + 1
        
    if random() < 0.65:
        A_votes = A_votes + 1
    else:
        B_votes = B_votes + 1
        
    if random() < 0.17:
        A_votes = A_votes + 1
    else:
        B_votes = B_votes + 1

    if A_votes > B_votes:
        A_wins = A_wins + 1
    else:
        B_wins = B_wins + 1

print(f"Probability A wins: {A_wins / num_of_trials}")
print(f"Probability B wins: {B_wins / num_of_trials}")
