def convert_cel_to_far(celcius):
    """Return the Fahrenheit converted from Celcius"""
    fahrenheit = (celcius * (9 / 5)) + 32
    return fahrenheit

def convert_far_to_cel(fahrenheit):
    """Return the Celcius converted from Fahrenheit"""
    celcius = (fahrenheit - 32) * (5 / 9)
    return celcius

fahrenheit = input("Enter a temperature in degrees F: ")
celcius = convert_far_to_cel(float(fahrenheit))
print(f"{fahrenheit} degrees F = {celcius: .2f} degrees C")

celcius = input("""
Enter a temperature in degrees Celsius: """)
fahrenheit = convert_cel_to_far(float(celcius))
print(f"{celcius} degrees C = {fahrenheit: .2f} degrees F")
